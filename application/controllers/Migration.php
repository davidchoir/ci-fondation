<?php

class Migration extends CI_Controller
{
    public function index()
    {
        $this->load->view('developer/migration');
    }

    public function migrate()
    {
        $this->load->library('migration');

        if ($this->migration->current() === FALSE) {
            show_error($this->migration->error_string());
        } else {
            $this->session->set_flashdata('success_migrate', 'Migrate success!');
            redirect('migration');
        }
    }
}