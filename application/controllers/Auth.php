<?php

class Auth extends MY_Controller {

    protected function middleware()
    {
        return array('auth|except:index,login');
    }

    public function index()
    {
        $this->load->view('auth/login');
    }

    public function login()
    {
        $auth = [
            'name' => 'Administrator',
			'username' => 'Admin',
			'email' => 'admin@example.com',
        ];
        
        $this->session->set_userdata('auth', $auth);
        redirect('auth/home');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

    public function home()
    {
        $this->load->view('auth/home');
    }
}