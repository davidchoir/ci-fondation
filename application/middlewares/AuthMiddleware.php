<?php

class AuthMiddleware {
    protected $controller;
    protected $ci;
    public $roles = array();
    public function __construct($controller, $ci)
    {
        $this->controller = $controller;
        $this->ci = $ci;
    }

    public function run(){

        if(is_null($this->ci->session->auth)) {
            show_error('You dont have a permission, please login correctly');
        }
    }
}