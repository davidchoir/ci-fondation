<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('dd'))
{
	function dd($param = NULL)
	{
		echo '<pre>'.print_r($param, TRUE).'</pre>';
        die();
	}
}